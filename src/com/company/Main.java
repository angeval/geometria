package com.company;

public class Main {

    public static void main(String[] args) {
        TelaUsuario tela = new TelaUsuario();
        tela.carregarFormas();
        while (tela.validarDados()){
            tela.informarDados();
        }
    }
}
