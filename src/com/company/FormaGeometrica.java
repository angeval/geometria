package com.company;

import java.util.List;

public abstract class FormaGeometrica implements AreaInterface{

    private List<Double> lados;
    private String nomeForma;

    public String getNomeForma() {
        return nomeForma;
    }

    public void setNomeForma(String nomeForma) {
        this.nomeForma = nomeForma;
    }

    public FormaGeometrica(List<Double> lados) {
        this.lados = lados;
    }

    public List<Double> getLados() {
        return lados;
    }

    public void setLados(List<Double> lados) {
        this.lados = lados;
    }
}
