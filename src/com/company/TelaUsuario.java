package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class TelaUsuario {
    public boolean fica=true;
    Scanner scanner = new Scanner(System.in);
    ArrayList<Double> valores = new ArrayList<>();
    HashMap<String, String> formas = new HashMap<>();

    public void carregarFormas(){
        formas.put("C","Círculo");
        formas.put("R","Retângulo");
        formas.put("T","Triângulo");
    }

    public void informarDados(){
        System.out.println("Qual forma deseja criar? Informe [C] para círculo / [T] para triângulo / [R] para retângulo / [S] para sair");
        fica=true;
        String forma = scanner.next();

        if (forma.equals("C") || forma.equals("R") || forma.equals("T")||
            forma.equals("c") || forma.equals("r") || forma.equals("t")) {
            capturaLados(forma);
            Calculadora c2 = new Calculadora();
            double area = c2.calcularAreaForma(forma, valores);
            System.out.println("Area " + formas.get(forma.toUpperCase()) + " " + area);
        }
        else if (forma.equals("S")||forma.equals("s")) {
                fica= false;
        }
        else {
            System.out.println("Informe uma forma válida!");
        }
    }

    public boolean validarDados() {
        if (!fica) {
            System.out.println("Saindo do app...");
            System.out.println("Fui..");
        }
        return fica;
    }

    public void capturaLados(String forma) {

        valores.clear();
        System.out.println("Informe os lados (1 para círculo / 2 para retângulo / 3 para triângulo): ");

        if (forma.equals("C") || forma.equals("c")) {
            double lado1 = scanner.nextDouble();
            valores.add(lado1);
        } else if (forma.equals("T") || forma.equals("t")) {
            double lado1 = scanner.nextDouble();
            valores.add(lado1);
            double lado2 = scanner.nextDouble();
            valores.add(lado2);
            double lado3 = scanner.nextDouble();
            valores.add(lado3);
        } else if (forma.equals("R") || forma.equals("r")) {
            double lado1 = scanner.nextDouble();
            valores.add(lado1);
            double lado2 = scanner.nextDouble();
            valores.add(lado2);
        }
    }
}
