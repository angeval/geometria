package com.company;

import java.util.ArrayList;

public class Calculadora {
    public double calcularAreaForma(String forma, ArrayList<Double> valores){

        double area=0;

        if (forma.equals("C")||forma.equals("c")) {
            Circulo circulo = new Circulo(valores);
            area = circulo.calcularArea();
        } else if (forma.equals("R")||forma.equals("r")) {
            Retangulo retangulo = new Retangulo(valores);
            area = retangulo.calcularArea();
        } else if (forma.equals("T")||forma.equals("t")) {
            Triangulo triangulo = new Triangulo(valores);
            if (!triangulo.validarTriangulo(valores.get(0), valores.get(1), valores.get(2))) {
                System.out.println("Triangulo inválido!!!");
            } else {
                area = triangulo.calcularArea();
            }
        }
        return area;
    }
}
