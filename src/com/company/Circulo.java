package com.company;

import java.util.List;

public class Circulo extends FormaGeometrica {

    public Circulo(List<Double> lados) {
        super(lados);
    }

    public double calcularArea() {
        double area = Math.pow(super.getLados().get(0), 2) * Math.PI;
        return area;
    }
}
