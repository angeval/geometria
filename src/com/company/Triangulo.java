package com.company;

import java.util.List;

public class Triangulo extends FormaGeometrica {
    public Triangulo(List<Double> lados) {
        super(lados);
    }

    @Override
    public double calcularArea() {
        double s = (super.getLados().get(0) + super.getLados().get(1) + super.getLados().get(2)) / 2;
        double area = Math.sqrt(s * (s - super.getLados().get(0)) * (s - super.getLados().get(1)) * (s - super.getLados().get(2)));
        return area;
    }

    public boolean validarTriangulo(double ladoA, double ladoB, double ladoC){
        if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA == true) {
            return true;
        }
        else {
            return false;
        }
    }
}
