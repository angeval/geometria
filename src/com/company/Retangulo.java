package com.company;

import java.util.List;

public class Retangulo extends FormaGeometrica {
    public Retangulo(List<Double> lados) {
        super(lados);
    }

    @Override
    public double calcularArea() {
        double area = super.getLados().get(0) * super.getLados().get(1);
        return area;
    }
}
