package com.company;

public interface AreaInterface {
    public abstract double calcularArea();
}
